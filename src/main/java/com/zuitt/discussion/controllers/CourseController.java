package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    // Create Course
    @RequestMapping(value="/courses", method= RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken, course);
        return new ResponseEntity<>("Course created successfully.", HttpStatus.CREATED);
    }

    // Get All Courses
    @RequestMapping(value="/courses", method=RequestMethod.GET)
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourse(), HttpStatus.OK);
    }

    // Delete a Course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable int courseid, @RequestHeader(value = "Authorization") String stringToken){
        return courseService.deleteCourse(courseid, stringToken);
    }

    // Update a Course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedPost(@PathVariable int courseid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseid, stringToken, course);
    }

    // Get Course by User
    @RequestMapping(value = "/myCourses", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }


}
