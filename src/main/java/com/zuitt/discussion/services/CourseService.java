package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    Iterable<Course> getCourse();

    ResponseEntity deleteCourse(int id, String stringToken);

    ResponseEntity updateCourse(int id, String stringToken, Course course);

    Iterable<Course> getMyCourses(String stringToken);

}
