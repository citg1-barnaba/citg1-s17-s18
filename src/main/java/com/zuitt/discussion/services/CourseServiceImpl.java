package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // Create a Course
    public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);

        courseRepository.save(newCourse);
    }

    // Get All Courses
    public Iterable<Course> getCourse() {
        return courseRepository.findAll();
    }

    // Delete Course
    public ResponseEntity deleteCourse(int id, String stringToken){
        Course courseForDeleting = courseRepository.findById(id).get();
        String courseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Unauthorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Update a Course
    public ResponseEntity updateCourse(int id, String stringToken, Course course){
        Course courseForUpdating = courseRepository.findById(id).get();
        String courseAuthorName = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);

            return  new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Unauthorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<Course> getMyCourses(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }

}
